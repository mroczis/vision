CREATE SEQUENCE public.bundle_id_seq;

CREATE TABLE public.bundle (
                id BIGINT NOT NULL DEFAULT nextval('public.bundle_id_seq'),
                name VARCHAR(200) NOT NULL,
                key VARCHAR(100) NOT NULL,
                start_item_id BIGINT,
                CONSTRAINT bundle_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.bundle_id_seq OWNED BY public.bundle.id;

CREATE SEQUENCE public.item_id_seq;

CREATE TABLE public.item (
                id BIGINT NOT NULL DEFAULT nextval('public.item_id_seq'),
                bundle BIGINT NOT NULL,
                name VARCHAR NOT NULL,
                url VARCHAR NOT NULL,
                CONSTRAINT item_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.item_id_seq OWNED BY public.item.id;

ALTER TABLE public.item ADD CONSTRAINT bundle_item_fk
FOREIGN KEY (bundle)
REFERENCES public.bundle (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.bundle ADD CONSTRAINT item_bundle_fk
FOREIGN KEY (start_item_id)
REFERENCES public.item (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;