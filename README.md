# Návod na spuštění

Nejdříve je potřeba si stáhnout dodatečné repozitáře (submoduly) pomocí příkazu:
```shell script
git submodule update --init --recursive
```

V případě, že dojde k aktualizaci některých ze závislostí je potřeba následně používat příkaz:
```shell script
git submodule update --recursive --remote
```

Tato složka poté obsahuje tři adresáře. Každý reprezentuje jednu část celku:
 - vision-be - Backend server (express-js)
 - vision-ui - Frontend server (angular)
 - vision-db - PostgreSQL databáze
 
Všechny tři lze spusit pomocí docker-compose. Prerekvizitou je nainstalovaný docker a docker-compose.
Pro spušení je potřeba v terminálu napsat:

```shell script
docker-compose up
```

V případě potřeby lze přidat ještě přepínač `-d`, aby se proces spustil asynchronně a 
běžel dále i po odlášení uživatele z bashe. Obraze lze znovu vytvořit přidáním přepínače `--build`.

Výchozí mapování portů ven z docker sítě je :
 - vision-be - `1111`
 - vision-ui - `80`
 - vision-db - `6768` (interní `5432`)
 
 Při spuštění na localhostu lze tak k výsledku přistoupit na adrese `http://localhost`.
 
# Nasezení na produkci

## Konfigurace reverzní proxy
V případě potřeby (když jsou na serveru porty `1111` a `80` už blokovány) je nutné přemapovat výstupní porty (viz níže) a upravit konfigurační soubory.
Předpokládejme následující skutečnosti:
 - vlastníme doménu `https://vision-ui.app`, máme nasazeno SSL,
 - chceme, aby backend byl dostupný na url adrese `https://vision-ui.app/api`,
 - samotná aplikace pro užviatele na adrese `https://vision-ui.app/app`,

Ideální je použít reverzní proxy (Apache, Nginx) a namapovat porty na relativní cesty serveru. 
V případě Apache je potřeba doplnit do konfiguračního souboru ve složce `sites-available` následující řádky:

```
ProxyPass /api/ http://localhost:1111/
ProxyPassReverse /api/ http://localhost:1111/
ProxyPass /app/ http://localhost:80/
ProxyPassReverse /app/ http://localhost:80/
```

Kofigurace ngix je obdobná, jen s jinou syntaxí.
Databázi není třeba tunelovat ven pro přístup přes HTTPS. Lze k ní v případě nutnosti přistoupit přes SSH tunel.
Tedy připojit se na server přes SSH a následně do databáze, která je dostupná na portu `6768`.

## Úprava zdrojových souborů
Po nakonfigurování serveru je třeba ještě změnit ve `vision-ui` URL adresu `vision-be` na reálnou, která je přistupná z internetu.
Tedy v soubouru `./vision-ui/src/enviroments/enviroment.prod.ts` přepsat `apiUrl` na `https://vision-ui.app/api`.
A konečně Docker obrazy znovu vytvořit s přepínačem `--build`.

# Změna portů
Kvůli provázanosti komponent změna portů není tak jednoduchá a vyžaduje změnu vícero souborů.
Přepokládejme, že nová hodnota portu je `XXXX`.

**Port PostgresSQL**
 - `docker-compose.yml` - přepsat `127.0.0.1:6768:5432` na `127.0.0.1:XXXX:5432`

**Port Frontendu**
 - `docker-compose.yml` - přepsat `127.0.0.1:80:80` na `127.0.0.1:XXXX:80`
 - `vision-ui/Dockerfile` - přepsat dva výskyty `80` na `XXXX`

**Port Backendu**
 - `docker-compose.yml` - přepsat `127.0.0.1:1111:1111` na `127.0.0.1:XXXX:1111`
 - `vision-be/Dockerfile` - přepsat jeden výskyt `1111` na `XXXX`
 - `vision-be/prod/.env` - přepsat jeden výskyt `1111` na `XXXX`
 - `vision-ui/src/enviroments/enviroment.prod.ts` - v případě závislosti na portu i zde přepsat `1111` na `XXXX`
 
Změny je nutné zpropagovat i do konfigurace reverzní proxy a znovu vytvořit docker obrazy!

# Zabezpečení
Backend server vyžaduje pro administraci Basic authorization. Heslo je nastaveno v souboru
`vision-be/prod/.env`.

Heslo do databáze je v souboru `vision-db/Dockerfile`, hodnota `POSTGRES_PASSWORD`. Měnit lze i uživatelské jméno a název
databáze. Změny je potřeba vždy promítnout do souboru `vision-be/prod/.env`. Host databáze musí zůstat `DB_HOST=vision-db`,
aby se server k ní mohl v Docker síti připojit.


